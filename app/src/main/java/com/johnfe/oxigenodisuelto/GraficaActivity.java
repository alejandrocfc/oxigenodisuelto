package com.johnfe.oxigenodisuelto;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;


import java.util.ArrayList;


public class GraficaActivity extends AppCompatActivity {


    LineChart lineChart;
    LineChart oxigenoVsTemp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafica);



        oxigenoVsTemp= (LineChart) findViewById(R.id.lineChart2);


        lineChart= (LineChart) findViewById(R.id.lineChart);


        ArrayList<Long> tiempo = new ArrayList<>();


        ArrayList<Entry> oxigeno = new ArrayList<>();
        ArrayList<Entry> oxigenoVsTemperatura = new ArrayList<>();
        ArrayList<Entry> temperatura = new ArrayList<>();

        long fechaReferencia= ListaDatos.datos.get(0).getTimestamp();

        for (Record dato : ListaDatos.datos  ){
            tiempo.add((dato.getTimestamp()-fechaReferencia)/1000);
            oxigeno.add(new  Entry((dato.getTimestamp()-fechaReferencia)/1000,dato.getOxigeno()));
            temperatura.add(new Entry((dato.getTimestamp()-fechaReferencia)/1000,dato.getTemperatura()));
        }

        for(int l=ListaDatos.datos.size()-1; l>=0; l--){
            Record dato = ListaDatos.datos.get(l);
            oxigenoVsTemperatura.add(new  Entry(dato.temperatura,dato.oxigeno));
        }

        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

        LineDataSet lineDataSetOxigeno= new LineDataSet(oxigeno, "Oxigeno");
        lineDataSetOxigeno.setDrawCircles(true);
        lineDataSetOxigeno.setColor(Color.BLUE);

        LineDataSet lineDataSetTemperatura= new LineDataSet(temperatura, "Temperatura");
        lineDataSetTemperatura.setDrawCircles(true);
        lineDataSetTemperatura.setColor(Color.RED);

        lineDataSets.add(lineDataSetOxigeno);
        lineDataSets.add(lineDataSetTemperatura);

        LineDataSet lineDataSetOxigenoVsTemp= new LineDataSet(oxigenoVsTemperatura, "Oxigeno vs Temperatura");
        lineDataSetOxigenoVsTemp.setDrawCircles(true);
        lineDataSetOxigenoVsTemp.setColor(Color.GREEN);


        lineChart.setData(new LineData(lineDataSets));
        Description description = new Description();
        description.setText("Oxígeno disuelto y Temperatura vs Tiempo");
        description.setTextSize(15);
        description.setTextColor(Color.DKGRAY);
        lineChart.setDescription(description);


        oxigenoVsTemp.setData(new LineData(lineDataSetOxigenoVsTemp));
        Description description1 = new Description();
        description1.setText("Oxígeno disuelto vs Temperatura");
        description1.setTextSize(15);
        description1.setTextColor(Color.DKGRAY);
        oxigenoVsTemp.setDescription(description1);




    }
}
