package com.johnfe.oxigenodisuelto;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Principal extends Fragment {

    TextView temperatura;
    TextView oxigeno;
    Button findRecords;
    Button buttonDateStart;
    Button buttonDateEnd;
    TextView dateStart;
    TextView dateEnd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.activity_main, container, false);
        Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yy hh:mm a");

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference records = database.getReference().child("records");

        findRecords= (Button) view.findViewById(R.id.findRecords);
        buttonDateStart = (Button) view.findViewById(R.id.buttonDateStart);
        buttonDateEnd = (Button) view.findViewById(R.id.buttonDateEnd);
        dateStart = (TextView) view.findViewById(R.id.dateStart);
        dateEnd = (TextView) view.findViewById(R.id.dateEnd);
        temperatura= (TextView) view.findViewById(R.id.txtTemperaturaMain);
        oxigeno= (TextView) view.findViewById(R.id.txtOxigeno1);

        dateStart.setText(formatter.format(date));
        dateEnd.setText(formatter.format(date));

        Query queryRef = records.orderByChild("timestamp").limitToLast(1);
        queryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Record record = data.getValue(Record.class);
                    temperatura.setText(record.temperatura+"°");
                    oxigeno.setText(record.oxigeno+"°");
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException());
            }
        });


        buttonDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragment(getActivity(),true);
                newFragment.show(getFragmentManager(),"datePicker");
            }
        });

        buttonDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new SelectDateFragment(getActivity(),false);
                newFragment.show(getFragmentManager(),"datePicker");
            }
        });

        findRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fechaInicio = dateStart.getText().toString();
                String fechaFin = dateEnd.getText().toString();
                DateFormat formatter = new SimpleDateFormat("dd/MM/yy hh:mm a");
                try {
                    Date dateInicio = (Date) formatter.parse(fechaInicio);
                    Date dateFin = (Date) formatter.parse(fechaFin);
                    Intent intent = new Intent(getContext(), ListaDatos.class);
                    Bundle bundle= new Bundle();
                    bundle.putString("fecha1",String.valueOf(dateInicio.getTime()));
                    bundle.putString("fecha2",String.valueOf(dateFin.getTime()));
                    intent.putExtras(bundle);
                    startActivity(intent);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });





        return view;


    }
}
