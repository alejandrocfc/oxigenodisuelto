package com.johnfe.oxigenodisuelto;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class Record {

    public Long timestamp;
    public Float oxigeno, temperatura;

    public Record() {
        // Default constructor required for calls to DataSnapshot.getValue(Record.class)
    }

    public Record(Long timestamp, Float oxigeno, Float temperatura) {
        this.timestamp = timestamp;
        this.oxigeno = oxigeno;
        this.temperatura = temperatura;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Float getOxigeno() {
        return oxigeno;
    }

    public void setOxigeno(Float oxigeno) {
        this.oxigeno = oxigeno;
    }

    public Float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }
}
// [END post_class]