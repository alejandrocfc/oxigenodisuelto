package com.johnfe.oxigenodisuelto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;

public class ListaDatos extends AppCompatActivity {

    public ListView list;
    public static  ArrayList<Record> datos = new ArrayList<Record>();
    public ListAdapter adapter;
    public Button btnGraficar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_datos);

        btnGraficar= (Button) findViewById(R.id.btnGrafica);

        btnGraficar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListaDatos.this, GraficaActivity.class);
                startActivity(intent);
            }
        });

        list = (ListView) findViewById(R.id.Lista);
        adapter = new ListAdapter(this);
        list.setAdapter(adapter);


        Bundle bundle;
        bundle= this.getIntent().getExtras();
        final String fechaInicio = bundle.getString("fecha1");
        final String fechaFin = bundle.getString("fecha2");
        DatabaseReference recordRef = FirebaseDatabase.getInstance().getReference("records");
        recordRef.orderByChild("timestamp")
                .startAt(Long.parseLong(fechaInicio))
                .endAt(Long.parseLong(fechaFin))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            datos.clear();
                            for (DataSnapshot data : dataSnapshot.getChildren()) {
                                Record actual = data.getValue(Record.class);
                                Record record = new Record();
                                record.setOxigeno(actual.oxigeno);
                                record.setTemperatura(actual.temperatura);
                                record.setTimestamp(actual.timestamp);
                                datos.add(record);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        else{
                            Log.w("NO EXISTE ","NADA");
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("", "Failed to read value.", error.toException());
                    }
                });
    }
}
