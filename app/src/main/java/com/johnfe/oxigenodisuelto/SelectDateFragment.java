package com.johnfe.oxigenodisuelto;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    public SelectDateFragment() {}

    private int day,month,year,hour,minutes;
    private Calendar calendar = Calendar.getInstance();
    private Calendar cal = Calendar.getInstance();
    private Activity activity;
    private Boolean date;

    public SelectDateFragment(Activity activity, Boolean flag) {
        this.activity = activity;
        this.date = flag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(this.activity, this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        day = dd;
        month = mm;
        year = yy;
        TimePickerDialog timePickerDialog = new TimePickerDialog(this.activity,this, hour, minute, false);
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        hour = hourOfDay;
        minutes = minute;
        populateSetDate();

    }

    public void populateSetDate() {
        TextView dateText;
        if(this.date){
            dateText = (TextView) activity.findViewById(R.id.dateStart);
        }else{
            dateText = (TextView) activity.findViewById(R.id.dateEnd);
        }
        cal.setTimeInMillis(0);
        cal.set(year, month, day, hour, minutes, 0);
        Date date = cal.getTime();
        DateFormat chosenData = new SimpleDateFormat("dd/MM/yy hh:mm a");
        dateText.setText(chosenData.format(date));
    }

}
